﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour
{
    [SerializeField]
    private GameObject m_BlockPrefab = null;

    [SerializeField]
    private GameObject m_AntimatterBlockPrefab = null;

    [SerializeField]
    private Rigidbody m_Rigidbody = null;

    [SerializeField]
    private AudioSource m_AudioSource = null;

    [SerializeField]
    private AudioClip m_ShapeStickClip = null;

    private List<Block> m_Blocks = new List<Block>();
    private bool m_IsAntimatterShape;
    private bool m_IsNextShape;
    private List<Shape> m_JoinedShapes = new List<Shape>();
    private Material m_Material;

    public bool IsAntimatterShape
    {
        get
        {
            return m_IsAntimatterShape;
        }
    }

    public List<Shape> JoinedShapes
    {
        get
        {
            return m_JoinedShapes;
        }
    }

    public Material Material
    {
        get
        {
            return m_Material;
        }
        set
        {
            m_Material = value;
        }
    }

    public Rigidbody Rigidbody
    {
        get
        {
            return m_Rigidbody;
        }
    }

    public void CreateBlocks()
    {
        ClearBlocks();

        GameObject blockPrefab;
        if (Random.value < 0.2f)
        {
            blockPrefab = m_AntimatterBlockPrefab;
            m_IsAntimatterShape = true;
        }
        else
        {
            blockPrefab = m_BlockPrefab;
        }

        for (int y = -1; y <= 1; y++)
        {
            for (int x = -1; x <= 1; x++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (Random.value < 0.5f)
                    {
                        continue;
                    }

                    GameObject blockObject = Instantiate(blockPrefab);

                    //if (m_IsAntimatterShape)
                    //{
                    //    blockObject = Instantiate(blockPrefab);
                    //}
                    //else
                    //{
                    //    blockObject = BlockPool.Borrow();
                    //}

                    if (blockObject != null)
                    {
                        blockObject.transform.SetParent(transform, false);
                        blockObject.transform.localPosition = new Vector3(x, y, z);

                        Block blockComponent = blockObject.GetComponent<Block>();
                        if (blockComponent != null)
                        {
                            blockComponent.Material = m_Material;
                            blockComponent.FixedJoint.connectedBody = m_Rigidbody;
                            blockComponent.ParentShape = this;
                            m_Blocks.Add(blockComponent);
                        }
                    }
                }
            }
        }
    }

    public void FadeIn(float alpha)
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(FadeInCoroutine(alpha));
        }
    }

    public void JoinTo(Shape shape)
    {
        if (m_IsAntimatterShape || shape == null || shape.IsAntimatterShape || m_JoinedShapes.Contains(shape) || shape.JoinedShapes.Contains(this))
        {
            return;
        }

        m_JoinedShapes.Add(shape);

        gameObject.AddComponent<FixedJoint>().connectedBody = shape.Rigidbody;

        if (m_AudioSource != null && m_ShapeStickClip != null && m_IsAntimatterShape == false)
        {
            m_AudioSource.pitch = Random.Range(0.5f, 1.25f);
            m_AudioSource.PlayOneShot(m_ShapeStickClip);
        }
    }

    public void SetAsActiveShape()
    {
        m_IsNextShape = false;

        SetLayer("Default");

        if (m_Rigidbody != null)
        {
            m_Rigidbody.isKinematic = false;
        }

        transform.rotation = Quaternion.identity;
    }

    public void SetAsNextShape()
    {
        m_IsNextShape = true;

        SetLayer("NextShape");

        if (m_Rigidbody != null)
        {
            m_Rigidbody.isKinematic = true;
        }
    }

    protected virtual void Update()
    {
        if (m_IsNextShape)
        {
            transform.Rotate(new Vector3(0.0f, 20.0f * Time.deltaTime, 0.0f));
        }

        if (transform.childCount == 0)
        {
            Tower.Detach(transform);
            Destroy(gameObject);
        }
    }

    private void ClearBlocks()
    {
        if (m_Blocks.Count == 0)
        {
            return;
        }

        for (int i = 0; i < m_Blocks.Count; i++)
        {
            BlockPool.Release(m_Blocks[i].gameObject);
        }
        m_Blocks.Clear();
    }

    private IEnumerator FadeInCoroutine(float alpha)
    {
        if (m_Material == null)
        {
            yield break;
        }

        float progress = 0.0f;

        while (progress <= 1.0f)
        {
            progress = Mathf.Clamp01(progress + Time.deltaTime);
            Color colour = m_Material.color;
            colour.a = Mathf.Lerp(0.0f, alpha, progress);
            m_Material.color = colour;

            yield return null;
        }
    }

    private void SetLayer(string layerName)
    {
        gameObject.layer = LayerMask.NameToLayer(layerName);

        foreach (Transform child in transform)
        {
            child.gameObject.layer = gameObject.layer;
        }
    }
}