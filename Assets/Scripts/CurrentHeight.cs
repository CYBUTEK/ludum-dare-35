﻿using UnityEngine;

public class CurrentHeight : MonoBehaviour
{
    [SerializeField]
    private TextMesh m_TextMesh = null;

    private float m_TargetHeight;

    protected virtual void Start()
    {
        UpdateTextMesh();
    }

    protected virtual void Update()
    {
        m_TargetHeight = Tower.Height;

        Vector3 position = transform.position;
        position.y = Mathf.Lerp(position.y, m_TargetHeight, Time.deltaTime);
        transform.position = position;

        UpdateTextMesh();
    }

    private void UpdateTextMesh()
    {
        if (m_TextMesh != null)
        {
            m_TextMesh.text = "Current Height " + m_TargetHeight.ToString("N1") + "m";
        }
    }
}