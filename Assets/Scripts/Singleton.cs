﻿namespace LudumDare35.Assets.Scripts
{
    using UnityEngine;

    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T s_Instance;

        public static T Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = FindObjectOfType<T>();

                    if (s_Instance == null)
                    {
                        Debug.LogError("Singleton type of " + typeof(T).Name + " does not exist.");
                    }
                }
                return s_Instance;
            }
        }
    }
}