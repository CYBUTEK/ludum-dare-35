﻿using System.Collections.Generic;
using LudumDare35.Assets.Scripts;
using UnityEngine;

public class Tower : Singleton<Tower>
{
    private static List<Transform> s_AttachedTransforms = new List<Transform>();
    private static float s_Height;

    public static float Height
    {
        get
        {
            return s_Height;
        }
    }

    public static void Attach(Transform child)
    {
        if (child == null || s_AttachedTransforms.Contains(child))
        {
            return;
        }

        child.SetParent(Instance.transform, true);
        s_AttachedTransforms.Add(child);
    }

    public static void Detach(Transform child)
    {
        if (child == null)
        {
            return;
        }

        s_AttachedTransforms.Remove(child);
    }

    protected virtual void Update()
    {
        s_Height = GetHeight();
    }

    private static float GetHeight()
    {
        float height = 0.0f;
        float tempHeight;

        for (int i = 0; i < s_AttachedTransforms.Count; i++)
        {
            tempHeight = s_AttachedTransforms[i].position.y;
            if (tempHeight > height)
            {
                height = tempHeight;
            }
        }

        return height;
    }
}