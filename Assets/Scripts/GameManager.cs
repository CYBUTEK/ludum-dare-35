﻿using System.Collections;
using LudumDare35.Assets.Scripts;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    private static float s_TimeTillDrop;
    private static bool s_IsApplicationFocused;

    [SerializeField]
    private GameObject m_ShapePrefab = null;

    [SerializeField]
    private GameObject m_OptionsMenuObject = null;

    [SerializeField]
    private GameObject m_ControlsMenuObject = null;

    [SerializeField]
    private Transform m_NextShapeTransform = null;

    [SerializeField]
    private AudioSource m_AudioSource = null;

    [SerializeField]
    private AudioClip m_SpawnClip = null;

    private Shape m_NextShape;

    public static bool IsApplicationFocused
    {
        get
        {
            return s_IsApplicationFocused;
        }
    }

    public static float TimeTillDrop
    {
        get
        {
            return s_TimeTillDrop;
        }
    }

    protected virtual void OnApplicationFocus(bool focus)
    {
        s_IsApplicationFocused = focus;
    }

    protected virtual IEnumerator Start()
    {
        while (true)
        {
            while (s_TimeTillDrop > 0.0f)
            {
                yield return null;
                s_TimeTillDrop -= Time.deltaTime;
            }
            s_TimeTillDrop = 3.0f;
            SpawnShape();
        }
    }

    protected virtual void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && m_OptionsMenuObject != null)
        {
            m_OptionsMenuObject.SetActive(true);
        }

        if ((m_OptionsMenuObject != null && m_OptionsMenuObject.activeInHierarchy) || (m_ControlsMenuObject != null && m_ControlsMenuObject.activeInHierarchy))
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;

            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;

            if (s_IsApplicationFocused)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }

    private void SetAsActiveShape(Shape shape)
    {
        if (shape == null)
        {
            return;
        }

        shape.transform.SetParent(null, false);
        shape.transform.position = new Vector3(Path.Instance.transform.position.x, Tower.Height + 15.0f, Path.Instance.transform.position.z);
        shape.SetAsActiveShape();
        shape.FadeIn(1.0f);

        if (m_AudioSource != null && m_SpawnClip != null)
        {
            m_AudioSource.PlayOneShot(m_SpawnClip);
        }
    }

    private void SetAsNextShape(Shape shape)
    {
        if (shape == null || m_NextShapeTransform == null)
        {
            return;
        }

        shape.transform.SetParent(m_NextShapeTransform, false);
        shape.SetAsNextShape();
        shape.FadeIn(0.75f);
    }

    private void SpawnShape()
    {
        if (m_ShapePrefab == null)
        {
            return;
        }

        SetAsActiveShape(m_NextShape);

        GameObject shapeObject = Instantiate(m_ShapePrefab);
        if (shapeObject != null)
        {
            Shape shapeComponent = shapeObject.GetComponent<Shape>();
            if (shapeComponent != null)
            {
                m_NextShape = shapeComponent;

                shapeComponent.Material = new Material(MaterialLibrary.GetRandomMaterial());
                shapeComponent.CreateBlocks();

                SetAsNextShape(shapeComponent);
            }
        }
    }
}