﻿using LudumDare35.Assets.Scripts;
using UnityEngine;

public class BlockPool : Singleton<BlockPool>
{
    [SerializeField]
    private GameObject m_Prefab = null;

    public ObjectPool m_Pool = new ObjectPool();

    public static GameObject Borrow()
    {
        return Instance.m_Pool.Borrow();
    }

    public static void Release(GameObject gameObject)
    {
        gameObject.SetActive(false);
        gameObject.transform.SetParent(null);
        gameObject.transform.position = Vector3.zero;
        gameObject.transform.rotation = Quaternion.identity;

        Instance.m_Pool.Release(gameObject);
    }

    protected virtual void Awake()
    {
        m_Pool.NewObjectFunc = () =>
        {
            if (m_Prefab != null)
            {
                return Instantiate(m_Prefab);
            }
            return null;
        };
    }
}