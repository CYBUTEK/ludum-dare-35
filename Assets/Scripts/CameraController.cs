﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    private static float s_Smoothing = 7.5f;
    private static float s_RotationSensitivity = 0.02f;
    private static float s_DistanceSensitivity = 0.3f;
    private float m_DistanceFromTarget = 10.0f;
    private float m_Rotation;
    private Vector3 m_TargetPosition;

    public static float DistanceSensitivity
    {
        get
        {
            return s_DistanceSensitivity;
        }
        set
        {
            s_DistanceSensitivity = value;
        }
    }

    public static float RotationSensitivity
    {
        get
        {
            return s_RotationSensitivity;
        }
        set
        {
            s_RotationSensitivity = value;
        }
    }

    public static float Smoothing
    {
        get
        {
            return s_Smoothing;
        }
        set
        {
            s_Smoothing = value;
        }
    }

    protected virtual void Update()
    {
        if (Input.GetMouseButton(1) && GameManager.IsApplicationFocused)
        {
            m_Rotation += Input.GetAxis("Mouse X") * s_RotationSensitivity;
            m_DistanceFromTarget = Mathf.Clamp(m_DistanceFromTarget - (Input.GetAxis("Mouse Y") * s_DistanceSensitivity), 7.5f, 30.0f);
        }

        m_TargetPosition = transform.position;

        m_TargetPosition.y = Tower.Height + (m_DistanceFromTarget * 0.66f);

        m_TargetPosition.x = Mathf.Cos(m_Rotation) * m_DistanceFromTarget;
        m_TargetPosition.z = Mathf.Sin(m_Rotation) * m_DistanceFromTarget;

        transform.position = Vector3.Lerp(transform.position, m_TargetPosition, Time.deltaTime * s_Smoothing);

        Vector3 target = new Vector3(0.0f, Tower.Height, 0.0f);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(target - transform.position), Time.deltaTime);

        Vector3 eulerAngles = transform.eulerAngles;
        eulerAngles.y = Quaternion.LookRotation(target - transform.position).eulerAngles.y;
        transform.eulerAngles = eulerAngles;
    }
}