﻿namespace LudumDare35.Assets.Scripts
{
    using System.Collections.Generic;
    using UnityEngine;

    public class MaterialLibrary : Singleton<MaterialLibrary>
    {
        [SerializeField]
        private List<Material> m_Materials = new List<Material>();

        public static Material GetRandomMaterial()
        {
            if (Instance.m_Materials.Count == 0)
            {
                return null;
            }

            return Instance.m_Materials[Random.Range(0, Instance.m_Materials.Count)];
        }
    }
}