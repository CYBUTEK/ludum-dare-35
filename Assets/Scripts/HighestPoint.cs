﻿using UnityEngine;

public class HighestPoint : MonoBehaviour
{
    private static float s_Height;

    [SerializeField]
    private bool m_Reset = false;

    [SerializeField]
    private TextMesh m_TextMesh = null;

    public static float Height
    {
        get
        {
            return s_Height;
        }
    }

    protected virtual void Start()
    {
        s_Height = PlayerPrefs.GetFloat("HighestPoint");

        UpdateTextMesh();
    }

    protected virtual void Update()
    {
        if (m_Reset)
        {
            PlayerPrefs.DeleteKey("HighestPoint");
        }

        if (Tower.Height > s_Height)
        {
            s_Height = Tower.Height;
            PlayerPrefs.SetFloat("HighestPoint", s_Height);
            UpdateTextMesh();
        }

        Vector3 position = transform.position;
        position.y = Mathf.Lerp(position.y, s_Height, Time.deltaTime);
        transform.position = position;
    }

    private void UpdateTextMesh()
    {
        if (m_TextMesh != null)
        {
            m_TextMesh.text = "Highest Point " + s_Height.ToString("N1") + "m";
        }
    }
}