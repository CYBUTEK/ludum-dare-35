﻿using UnityEngine;

public class Menu : MonoBehaviour
{
    protected virtual void OnDisable()
    {
        Cursor.visible = false;
    }

    protected virtual void OnEnable()
    {
        Cursor.visible = true;
    }
}