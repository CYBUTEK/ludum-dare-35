﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UIQuitGame : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        print("Quitting Application");
        Application.Quit();
    }
}