﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class UIVolume : MonoBehaviour
{
    [SerializeField]
    private Slider m_Slider = null;

    [SerializeField]
    private AudioMixer m_Mixer = null;

    [SerializeField]
    private string m_ParemeterName = string.Empty;

    public float Volume
    {
        get
        {
            if (m_Slider == null)
            {
                return 0.0f;
            }
            return m_Slider.value;
        }
        set
        {
            m_Mixer.SetFloat(m_ParemeterName, value);

            if (m_Slider != null)
            {
                m_Slider.value = value;
            }
        }
    }

    protected virtual void Start()
    {
        float volume;
        if (m_Mixer.GetFloat(m_ParemeterName, out volume))
        {
            Volume = volume;
        }
    }
}