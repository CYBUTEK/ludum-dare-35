﻿using UnityEngine;
using UnityEngine.UI;

public class UICameraSmoothing : MonoBehaviour
{
    [SerializeField]
    private Slider m_Slider = null;

    public float Value
    {
        get
        {
            if (m_Slider == null)
            {
                return 0.0f;
            }
            return m_Slider.value;
        }
        set
        {
            CameraController.Smoothing = value;

            if (m_Slider != null)
            {
                m_Slider.value = value;
            }
        }
    }

    protected virtual void Start()
    {
        Value = CameraController.Smoothing;
    }
}