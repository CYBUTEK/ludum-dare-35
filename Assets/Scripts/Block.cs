﻿using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer m_MeshRenderer = null;

    [SerializeField]
    private FixedJoint m_FixedJoint = null;

    [SerializeField]
    private AudioClip m_GroundHitClip = null;

    [SerializeField]
    private AudioSource m_AudioSource = null;

    private bool m_HasBeenVisible;
    private Shape m_ParentShape;

    public AudioSource AudioSource
    {
        get
        {
            return m_AudioSource;
        }
    }

    public FixedJoint FixedJoint
    {
        get
        {
            return m_FixedJoint;
        }
    }

    public Material Material
    {
        get
        {
            if (m_MeshRenderer == null)
            {
                return null;
            }
            return m_MeshRenderer.sharedMaterial;
        }
        set
        {
            if (m_MeshRenderer != null)
            {
                m_MeshRenderer.sharedMaterial = value;
            }
        }
    }

    public Shape ParentShape
    {
        get
        {
            return m_ParentShape;
        }
        set
        {
            m_ParentShape = value;
        }
    }

    protected virtual void OnBecameInvisible()
    {
        if (m_HasBeenVisible)
        {
            //BlockPool.Release(gameObject);
            Destroy(gameObject);
        }
    }

    protected virtual void OnBecameVisible()
    {
        m_HasBeenVisible = true;
    }

    protected virtual void OnCollisionEnter(Collision other)
    {
        if (other.transform.parent == transform.parent)
        {
            return;
        }

        if (other.transform.CompareTag("Platform"))
        {
            PlayFloorHitSound(other.relativeVelocity.magnitude);
        }

        Block block = other.gameObject.GetComponent<Block>();
        if (block != null)
        {
            Tower.Attach(ParentShape.transform);
            ParentShape.JoinTo(block.ParentShape);
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }

    protected void PlayFloorHitSound(float relativeMagnitude)
    {
        if (m_AudioSource != null && m_GroundHitClip != null)
        {
            m_AudioSource.pitch = Random.Range(0.8f, 1.2f);
            m_AudioSource.PlayOneShot(m_GroundHitClip, Mathf.Clamp01(relativeMagnitude * 0.2f));
        }
    }
}