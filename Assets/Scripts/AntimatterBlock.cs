﻿using UnityEngine;

public class AntimatterBlock : Block
{
    [SerializeField]
    private AudioClip m_AnihilationClip = null;

    protected override void OnCollisionEnter(Collision other)
    {
        if (other.transform.parent == transform.parent || other.gameObject.CompareTag("Block") == false)
        {
            if (other.transform.CompareTag("Platform"))
            {
                PlayFloorHitSound(other.relativeVelocity.magnitude);
            }
            return;
        }

        if (AudioSource != null && m_AnihilationClip != null)
        {
            AudioSource.PlayClipAtPoint(m_AnihilationClip, transform.position);
        }

        Destroy(other.gameObject);
        Destroy(gameObject);

        CreateExplosion();
    }

    private void CreateExplosion()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 5.0f);

        foreach (Collider hit in colliders)
        {
            Rigidbody otherRigidbody = hit.GetComponent<Rigidbody>();

            if (otherRigidbody != null)
            {
                otherRigidbody.AddExplosionForce(250.0f, transform.position, 5.0f, -1.0f);
            }
        }
    }
}