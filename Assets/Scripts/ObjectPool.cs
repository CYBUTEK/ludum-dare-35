﻿namespace LudumDare35.Assets.Scripts
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class ObjectPool
    {
        private Func<GameObject> m_NewObjectFunc;
        private Stack<GameObject> m_Objects = new Stack<GameObject>();

        public Func<GameObject> NewObjectFunc
        {
            get
            {
                return m_NewObjectFunc;
            }
            set
            {
                m_NewObjectFunc = value;
            }
        }

        public GameObject Borrow()
        {
            if (m_Objects.Count > 0)
            {
                return m_Objects.Pop();
            }
            if (m_NewObjectFunc != null)
            {
                return m_NewObjectFunc.Invoke();
            }
            return null;
        }

        public void Release(GameObject gameObject)
        {
            m_Objects.Push(gameObject);
        }
    }
}