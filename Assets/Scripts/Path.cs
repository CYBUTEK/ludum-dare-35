﻿using System.Collections;
using LudumDare35.Assets.Scripts;
using UnityEngine;

public class Path : Singleton<Path>
{
    private static float s_Sensitivity = 0.1f;
    private static float s_Smoothing = 10.0f;

    [SerializeField]
    private Material m_Material = null;

    private Vector3 m_DriftDirection;
    private Vector3 m_TargetPosition;

    public static float Sensitivity
    {
        get
        {
            return s_Sensitivity;
        }
        set
        {
            s_Sensitivity = value;
        }
    }

    public static float Smoothing
    {
        get
        {
            return s_Smoothing;
        }
        set
        {
            s_Smoothing = value;
        }
    }

    protected virtual void Start()
    {
        StartCoroutine(DriftTargetCoroutine());
    }

    protected virtual void Update()
    {
        if (Input.GetMouseButton(1) == false && GameManager.IsApplicationFocused)
        {
            Vector2 mousePositionDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")) * s_Sensitivity;

            Vector3 forward = Camera.main.transform.forward;
            forward.y = 0.0f;
            forward *= mousePositionDelta.y;

            Vector3 right = Camera.main.transform.right;
            right.y = 0.0f;
            right *= mousePositionDelta.x;

            m_TargetPosition += forward + right;
        }

        m_TargetPosition += m_DriftDirection * Time.deltaTime;
        m_TargetPosition.y = (Tower.Height + 100.0f) * 0.5f;

        transform.position = Vector3.Lerp(transform.position, m_TargetPosition, Time.deltaTime * s_Smoothing);
        transform.localScale = new Vector3(3.0f, transform.position.y * 2.0f, 3.0f);

        UpdateMaterial();
    }

    private IEnumerator DriftTargetCoroutine()
    {
        while (true)
        {
            m_DriftDirection = (new Vector3(Random.Range(-5.0f, 5.0f), 0.0f, Random.Range(-5.0f, 5.0f)) - transform.position).normalized;
            yield return new WaitForSeconds(3.0f);
        }
    }

    private void UpdateMaterial()
    {
        if (m_Material == null)
        {
            return;
        }

        Color colour = m_Material.color;
        colour.a = Mathf.Lerp(0.25f, 0.5f, GameManager.TimeTillDrop / 3.0f);
        m_Material.color = colour;
    }
}